# README #

**start_cpp_func.py** - Python-скрипт в котором происходит вызов С++ функций

**func.cpp** - Файл с C++ функциями, которые будут вызываться в Python-скрипте

**libfunc.so** - Файл собранной из *func.cpp* shared library

**file.txt** - Файл для тестирования загрузки из файла для *func.cpp*

Команда для самостоятельной сборки shared library:

```
#!bash

g++ -shared -o libfunc.so -fPIC func.cpp
```