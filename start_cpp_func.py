from ctypes import *

libc = CDLL("libfunc.so")

result = libc.file_read("file.txt")
print "Result file_read('file.txt'): ", bool(result)

sum = libc.add(5, 2)
print "Result add(5, 2): ", int(sum)

sub = libc.sub(5, 2)
print "Result sub(5, 2): ", int(sub)
