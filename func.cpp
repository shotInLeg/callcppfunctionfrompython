#include <iostream>
#include <fstream>
using namespace std;

extern "C" bool file_read(const char * filepath)
{
	char buff[1024];
	ifstream file(filepath);

	if (!file.is_open())
	{
		cout << "Файл не может быть открыт!\n";
		return false;
	}
	else
	{
		while(!file.eof())
		{
			file.getline(buff, 1024);
			cout << buff << endl;
		}
		file.close();
	}

	return true;
} 

extern "C" int add(int a, int b)
{
	return a + b;
}

extern "C" int sub(int a, int b)
{
	return a - b;
}
